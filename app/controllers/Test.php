<?php
namespace app\controllers;
use app\core\BaseController;
use app\models\TestData;

class Test extends BaseController
{
    public function actionIndex($params)
    {
        $model = new TestData();
        //return $this->render('viewfile', array_merge($params, $model->getData(), $this->app->config));
        return $this->render('viewfile', ['data' => $model->getAll()]);
    }
    
    public function actionCreate()
    {
        $model = new TestData();
        
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post' && !empty($_POST)) {
            $model->insert($_POST);
            
            return $this->actionIndex(null);
            
        } else {
            return $this->render('createForm');
        }
    }
}