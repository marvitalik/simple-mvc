<?php
namespace app\core\db;
use app\core\BaseModel;
use app\core\App;

abstract class Mysql extends BaseModel
{
    public $tableName;
    protected static $dbh = null; 
    
    public function __construct()
    {
        if (is_null(self::$dbh)) {
            $config = App::$userConfig['db'];
            $dbh = new \PDO('mysql:host='.$config['host'].';dbname='.$config['name'], $config['user'], $config['pass']);
            self::$dbh = $dbh;
        }
    }
    
    public function insert($data)
    {
        $data['created_at'] = time();
        $insertQuery = 'INSERT INTO `'.$this->tableName.'` (`'.implode('`, `', array_keys($data)).'`) VALUES (:'.implode(', :', array_keys($data)).')';
        $sth = self::$dbh->prepare($insertQuery);
        if ($sth->execute($data)) {
            return true;
        } else {
            throw new \Exception($sth->errorInfo()[2]);
        }
    }
    
    abstract public function getAll();
   
}