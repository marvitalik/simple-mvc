<?php
namespace app\core;

class App
{
    public static $config;
    public static $userConfig;
        
    public function __construct($config, $userConfig) 
    {
        self::$config = $config;
        self::$userConfig = $userConfig;
    }
    
    public static function fullPath()
    {
        return __DIR__.'/..';
    }
    public static function webPath()
    {
        return (empty($_SERVER['CONTEXT_PREFIX']) ? '' : $_SERVER['CONTEXT_PREFIX']);
    }
    
    public function run()
    {
        $baseLength = strlen(self::webPath());
        if (empty($_SERVER['QUERY_STRING'])) {
            $queryLength = 0;
            $arguments = [];
        } else {
            $queryLength = strlen($_SERVER['QUERY_STRING']) + 1;
            $arguments = explode('&', $_SERVER['QUERY_STRING']);
        }
        $url = explode('/', substr($_SERVER['REQUEST_URI'], $baseLength, (strlen($_SERVER['REQUEST_URI']) - $queryLength - $baseLength)));
        $controllerName = 'app\\controllers\\'.(empty($url[1]) ? 'Test' : ucfirst(strtolower($url[1])));
        $actionName = 'action'.(empty($url[2]) ? 'Index' : ucfirst(strtolower($url[2])));
        
        $params = [];
        if (!empty($arguments)) {
            foreach ($arguments as $argument) {
                $arg = explode('=', $argument);
                $params[$arg[0]] = $arg[1];
            }
        }
       
        if (class_exists($controllerName)) {
            $controller = new $controllerName($this);
            return $controller->$actionName($params);
        } else {
            throw new \Exception('Bad URL');
        }
    }
}