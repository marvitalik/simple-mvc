<?php
namespace app\core;
use app\core\App;

class BaseController
{
    protected $app;
    
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    public function render($viewName, $params = [], $layoutName = '')
    {
        if (empty($layoutName)) {
            $layoutName = App::$config['defaultLayout'];
        }
        $viewFile = App::fullPath().'/views/'.$viewName.'.php';
        $layoutFile = App::fullPath().'/../web/layouts/'.$layoutName.'.php';
        $webRoot = App::webPath();

        foreach (['css', 'js'] as $asset) {
            $path = $webRoot.'/web/'.$asset.'/';
            $files = $asset.'Files';
            foreach (App::$config[$asset] as $item) {
                ${$files}[] = $path.$item.'.'.$asset;
            }
        }

        if (file_exists($viewFile) && file_exists($layoutFile)) {
            extract($params);
            ob_start();
            include($viewFile);
            $content = ob_get_clean();
            include($layoutFile);
        } else {
            throw new \Exception('View file not found');
        }
    }
}