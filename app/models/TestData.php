<?php
namespace app\models;
use app\core\db\Mysql;

class TestData extends Mysql
{
    public $tableName = 'testData';
    
    public function getData()
    {
        return (['a' => 'A', 'b' => 'B']);
    }
    
    public function getAll()
    {
        $sth = self::$dbh->query('SELECT * FROM `'.$this->tableName.'`');
        return $sth->fetchAll();
    }
}