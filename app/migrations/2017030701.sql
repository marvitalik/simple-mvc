CREATE TABLE IF NOT EXISTS `testData` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    `value` float(8,2),
    `date` date,
    `created_at` int(10) UNSIGNED NOT NULL,    
    `updated_at` int(10) UNSIGNED DEFAULT NULL,
    
    PRIMARY KEY (`id`),
    KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;