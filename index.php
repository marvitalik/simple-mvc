<?php

require_once(__DIR__.'/app/config.php');
require_once(__DIR__.'/userConfig.php');
require_once(__DIR__.'/vendor/autoload.php');

$app = new app\core\App($config, $userConfig);
$app->run();
