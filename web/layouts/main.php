<head>
  <?php foreach ($cssFiles as $css) : ?>
    <link href="<?= $css ?>" rel="stylesheet">
  <?php endforeach; ?>
    
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>

<span class="test">header</span>

<div class="container">
    <?= $content ?>
</div>

footer

<?php foreach ($jsFiles as $js) : ?>
  <script src="<?= $js ?>"></script>
<?php endforeach; ?>
